%bcond_without gui
%bcond_with default_editor
%bcond_with libsodium_crypt

%define WITH_SELINUX 1

%if %{with gui}
%define desktop_file 1
%else
%define desktop_file 0
%endif

%if %{desktop_file}
%define desktop_file_utils_version 0.2.93
%endif

%define withnetbeans 1
%define withvimspell 0
%define withhunspell 0
%define withlua 1
%define withperl 1
%define withruby 1
# https://github.com/vim/vim/pull/3507
%define _fortify_level 1
%define vimdir vim90

Summary:  The VIM editor
URL:      http://www.vim.org/
Name:     vim
Version:  9.0.2092
Release:  10%{?dist}
License:  Vim and MIT and GPL v2+
Source0:  https://github.com/vim/vim/archive/refs/tags/v%{version}.tar.gz
Source1:  virc
Source2:  vimrc
# created from https://commons.wikimedia.org/wiki/File:Vimlogo.svg, GPL v2+ version
Source3:  gvim16.png
Source4:  gvim32.png
Source5:  gvim48.png
Source6:  gvim64.png
Source8:  macros.vim
Source9:  vim-default-editor.sh
Source12: view_wrapper
Source13: vi_wrapper

%if %{withvimspell}
Source14: vim-spell-files.tar.bz2
%endif

Patch0001: 0001-backport-CVE-2023-48231-patch-9.0.2106-security-Use-.patch
Patch0002: 0002-backport-CVE-2023-48232-patch-9.0.2107-security-FPE-.patch
Patch0003: 0003-backport-CVE-2023-48233-patch-9.0.2108-security-over.patch
Patch0004: 0004-backport-CVE-2023-48234-patch-9.0.2109-security-over.patch
Patch0005: 0005-backport-CVE-2023-48235-patch-9.0.2110-security-over.patch
Patch0006: 0006-backport-CVE-2023-48236-patch-9.0.2111-security-over.patch
Patch0007: 0007-backport-CVE-2023-48237-patch-9.0.2112-security-over.patch
Patch0008: 0008-backport-CVE-2023-48706-patch-9.0.2121-security-use-.patch
Patch0009: 0009-backport-CVE-2024-22667-patch-9.0.2142-security-stac.patch
# CVE-2024-41957
Patch0010: 0010-security-use-after-free-in-tagstack_clear_entry.patch
# CVE-2024-43374
Patch0011: 0011-patch-9.1.0678-security-use-after-free-in-alist_add.patch
# CVE-2024-43802
Patch0012: 0012-patch-9.1.0697-security-heap-buffer-overflow-in-ins_.patch
# CVE-2024-47814, commit: 51b62387be93c65fa56bbabe1c3
Patch0013: 0013-patch-9.1.0764-security-use-after-free-when-closing-.patch
# CVE-2025-22134, commit: c9a1e257f1630a0866447e53a564f7ff96a80ead
Patch0014: 0014-patch-9.1.1003-security-heap-buffer-overflow-with-vi.patch
Patch0015: fixed-CVE-2025-1215.patch
Patch0016: fixed-CVE-2025-26603.patch

Patch3000: vim-7.3-manpage-typo-668894-675480.patch
Patch3001: vim-manpagefixes-948566.patch
Patch3002: vim-7.4-globalsyntax.patch
Patch3003: vim-python3-tests.patch
Patch3004: vim-8.0-copy-paste.patch
Patch3005: vim-7.0-fixkeys.patch


%if %{withhunspell}
Patch3008: vim-7.0-hunspell.patch
BuildRequires: hunspell-devel
%endif

BuildRequires: autoconf, gcc, gettext, glibc-gconv-extra, gpm-devel, libacl-devel, make, ncurses-devel, python3-devel
# for tests
BuildRequires: coreutils, libtool

%if %{desktop_file}
BuildRequires: desktop-file-utils >= %{desktop_file_utils_version}
Requires: desktop-file-utils
%endif

%if %{WITH_SELINUX}
BuildRequires: libselinux-devel
%endif

%if %{with libsodium_crypt}
BuildRequires: libsodium-devel
%endif

%if "%{withlua}" == "1"
BuildRequires: lua-devel
%endif

%if "%{withperl}" == "1"
BuildRequires: perl-devel
BuildRequires: perl-generators
BuildRequires: perl(ExtUtils::Embed)
BuildRequires: perl(ExtUtils::ParseXS)
%endif

%if "%{withruby}" == "1"
BuildRequires: ruby
BuildRequires: ruby-devel
%endif

%description
VIM (VIsual editor iMproved) is an updated and improved version of the
vi editor.  Vi was the first real screen-based editor for UNIX, and is
still very popular.  VIM improves on vi by adding new features:
multiple windows, multi-level undo, block highlighting and more.

%package common
Summary: The common files needed by any version of the VIM editor
Requires: %{name}-data = %{version}-%{release}
Requires: %{name}-filesystem
Provides: vim-toml = %{version}-%{release}

%description common
VIM (VIsual editor iMproved) is an updated and improved version of the
vi editor.  Vi was the first real screen-based editor for UNIX, and is
still very popular.  VIM improves on vi by adding new features:
multiple windows, multi-level undo, block highlighting and more.  The
vim-common package contains files which every VIM binary will need in
order to run.

If you are installing vim-enhanced or vim-X11, you'll also need
to install the vim-common package.

%package spell
Summary: The dictionaries for spell checking. This package is optional
Requires: vim-common = %{version}-%{release}

%description spell
This subpackage contains dictionaries for vim spell checking in
many different languages.

%package minimal
Summary: A minimal version of the VIM editor
Provides: vi
Provides: %{_bindir}/vi
Requires: %{name}-data = %{version}-%{release}

%description minimal
VIM (VIsual editor iMproved) is an updated and improved version of the
vi editor.  Vi was the first real screen-based editor for UNIX, and is
still very popular.  VIM improves on vi by adding new features:
multiple windows, multi-level undo, block highlighting and more. The
vim-minimal package includes a minimal version of VIM, providing
the commands vi, view, ex, rvi, and rview. NOTE: The online help is
only available when the vim-common package is installed.

%package enhanced
Summary: A version of the VIM editor which includes recent enhancements
Provides: bundled(libvterm)
Provides: vim
Provides: vim(plugins-supported)
Provides: %{_bindir}/mergetool
Provides: %{_bindir}/vim
Requires: vim-common = %{version}-%{release}
Requires: which
%if "%{withlua}" == "1"
Suggests: lua-libs
%endif

%if "%{withperl}" == "1"
Suggests: perl-devel
%endif

Suggests: python3
Suggests: python3-libs

%if "%{withruby}" == "1"
Suggests: ruby
Suggests: ruby-libs
%endif

%description enhanced
VIM (VIsual editor iMproved) is an updated and improved version of the
vi editor.  Vi was the first real screen-based editor for UNIX, and is
still very popular.  VIM improves on vi by adding new features:
multiple windows, multi-level undo, block highlighting and more.  The
vim-enhanced package contains a version of VIM with extra, recently
introduced features like Python and Perl interpreters.

Install the vim-enhanced package if you'd like to use a version of the
VIM editor which includes recently added enhancements like
interpreters for the Python and Perl scripting languages.  You'll also
need to install the vim-common package.

%package filesystem
Summary: VIM filesystem layout
BuildArch: noarch

%Description filesystem
This package provides some directories which are required by other
packages that add vim files, p.e.  additional syntax files or filetypes.

%if %{with gui}
%package X11
Summary: The VIM version of the vi editor for the X Window System - GVim
BuildRequires: gtk3-devel
BuildRequires: libICE-devel
BuildRequires: libSM-devel
BuildRequires: libX11-devel
BuildRequires: libXpm-devel
BuildRequires: libXt-devel
BuildRequires: libappstream-glib
BuildRequires: libcanberra-devel

Provides: gvim
Provides: vim(plugins-supported)
Provides: %{_bindir}/mergetool
Provides: %{_bindir}/gvim
Requires: gtk3
Requires: hicolor-icon-theme
Requires: libattr >= 2.4
Requires: vim-common = %{version}-%{release} 
%if "%{withlua}" == "1"
Suggests: lua-libs
%endif

%if "%{withperl}" == "1"
Suggests: perl-devel
%endif

Suggests: python3
Suggests: python3-libs

%if "%{withruby}" == "1"
Suggests: ruby
Suggests: ruby-libs
%endif

%description X11
VIM (VIsual editor iMproved) is an updated and improved version of the
vi editor.  Vi was the first real screen-based editor for UNIX, and is
still very popular.  VIM improves on vi by adding new features:
multiple windows, multi-level undo, block highlighting and
more. VIM-X11 is a version of the VIM editor which will run within the
X Window System.  If you install this package, you can run VIM as an X
application with a full GUI interface and mouse support by command gvim.

Install the vim-X11 package if you'd like to try out a version of vi
with graphics and mouse capabilities.  You'll also need to install the
vim-common package.
%endif

%package data
Summary: Shared data for Vi and Vim
BuildArch: noarch

%description data
The subpackage is used for shipping files and directories, which need to be
shared between vim-minimal and vim-common packages.

%if %{with default_editor}
%package default-editor
Summary: Set vim as the default editor
BuildArch: noarch
Conflicts: system-default-editor
Provides: system-default-editor
Requires: vim-enhanced

%description default-editor
This subpackage contains files needed to set Vim as the default editor.
%endif



%prep
%setup -q -b 0
chmod -x runtime/tools/mve.awk
perl -pi -e "s,bin/nawk,bin/awk,g" runtime/tools/mve.awk

%if %{withvimspell}
%{__tar} xjf %{SOURCE14}
%endif

%autopatch -p1

%build
cd src
autoconf

export CFLAGS="%{optflags} -D_GNU_SOURCE -D_FILE_OFFSET_BITS=64 -D_FORTIFY_SOURCE=2"
export CXXFLAGS="%{optflags} -D_GNU_SOURCE -D_FILE_OFFSET_BITS=64 -D_FORTIFY_SOURCE=2"

cp -f os_unix.h os_unix.h.save

perl -pi -e "s/vimrc/virc/"  os_unix.h
%configure CFLAGS="${CFLAGS} -DSYS_VIMRC_FILE='\"/etc/virc\"'" \
  --prefix=%{_prefix} --with-features=tiny --with-x=no \
  --enable-multibyte \
  --disable-netbeans \
%if %{WITH_SELINUX}
  --enable-selinux \
%else
  --disable-selinux \
%endif
  --disable-pythoninterp --disable-perlinterp --disable-tclinterp \
  --with-tlib=ncurses --enable-gui=no --disable-gpm --exec-prefix=/ \
  --with-compiledby="<jeremiazhao@tencent.com>" \
  --with-modified-by="<jeremiazhao@tencent.com>" \
  --enable-fips-warning \
  --enable-fail-if-missing \
  --disable-canberra \
  --disable-libsodium

%make_build
cp vim minimal-vim
make clean

mv -f os_unix.h.save os_unix.h

%if %{with gui}

%configure CFLAGS="${CFLAGS} -DSYS_VIMRC_FILE='\"/etc/vimrc\"'" \
  --with-features=huge \
  --enable-python3interp=dynamic \
  --disable-tclinterp --with-x=yes \
  --enable-xim --enable-multibyte \
  --with-tlib=ncurses \
  --enable-gtk3-check --enable-gui=gtk3 \
  --enable-fips-warning \
  --with-compiledby="<jeremiazhao@tencent.com>" --enable-cscope \
  --with-modified-by="<jeremiazhao@tencent.com>" \
  %if "%{withnetbeans}" == "1"
  --enable-netbeans \
  %else
  --disable-netbeans \
  %endif
  %if %{WITH_SELINUX}
  --enable-selinux \
  %else
  --disable-selinux \
  %endif
  %if "%{withperl}" == "1"
  --enable-perlinterp=dynamic \
  %else
  --disable-perlinterp \
  %endif
  %if "%{withruby}" == "1"
  --enable-rubyinterp=dynamic \
  %else
  --disable-rubyinterp \
  %endif
  %if "%{withlua}" == "1"
  --enable-luainterp=dynamic \
  %else
  --disable-luainterp \
  %endif
  %if %{with libsodium_crypt}
  --enable-libsodium \
  %else
  --disable-libsodium \
  %endif
  --enable-fail-if-missing \
  --enable-canberra

%make_build

cp vim gvim
make clean
%endif


%configure CFLAGS="${CFLAGS} -DSYS_VIMRC_FILE='\"/etc/vimrc\"'" \
 --prefix=%{_prefix} --with-features=huge \
 --enable-python3interp=dynamic \
 --disable-tclinterp \
 --with-x=no \
 --enable-gui=no --exec-prefix=%{_prefix} --enable-multibyte \
 --enable-cscope --with-modified-by="<jeremiazhao@tencent.com>" \
 --with-tlib=ncurses \
 --enable-fips-warning \
 --with-compiledby="<jeremiazhao@tencent.com>" \
%if "%{withnetbeans}" == "1"
  --enable-netbeans \
%else
  --disable-netbeans \
%endif
%if %{WITH_SELINUX}
  --enable-selinux \
%else
  --disable-selinux \
%endif
%if "%{withperl}" == "1"
  --enable-perlinterp=dynamic \
%else
  --disable-perlinterp \
%endif
%if "%{withruby}" == "1"
  --enable-rubyinterp=dynamic \
%else
  --disable-rubyinterp \
%endif
%if "%{withlua}" == "1"
  --enable-luainterp=dynamic \
%else
  --disable-luainterp \
%endif
%if %{with libsodium_crypt}
  --enable-libsodium \
%else
  --disable-libsodium \
%endif
  --enable-fail-if-missing \
  --disable-canberra

%make_build
cp vim enhanced-vim



%install
mkdir -p %{buildroot}/%{_bindir}
mkdir -p %{buildroot}/%{_datadir}/%{name}/vimfiles/{after,autoload,colors,compiler,doc,ftdetect,ftplugin,indent,keymap,lang,plugin,print,spell,syntax,tutor}
mkdir -p %{buildroot}/%{_datadir}/%{name}/vimfiles/after/{autoload,colors,compiler,doc,ftdetect,ftplugin,indent,keymap,lang,plugin,print,spell,syntax,tutor}
cp runtime/doc/uganda.txt LICENSE
rm -f README*.info


cd src
%make_install BINDIR=%{_bindir} STRIP=/bin/true
rm -f %{buildroot}%{_bindir}/{vim,view}
mkdir -p %{buildroot}%{_datadir}/icons/hicolor/{16x16,32x32,48x48,64x64}/apps
mkdir -p %{buildroot}%{_libexecdir}
install -m755 minimal-vim %{buildroot}%{_libexecdir}/vi
install -m755 enhanced-vim %{buildroot}%{_bindir}/vim
install -m755 %{SOURCE12} %{buildroot}%{_bindir}/view
install -m755 %{SOURCE13} %{buildroot}%{_bindir}/vi

%if %{with gui}
make installgtutorbin  DESTDIR=%{buildroot} BINDIR=%{_bindir}
install -m755 gvim %{buildroot}%{_bindir}/gvim
install -p -m644 %{SOURCE3} \
   %{buildroot}%{_datadir}/icons/hicolor/16x16/apps/gvim.png
install -p -m644 %{SOURCE4} \
   %{buildroot}%{_datadir}/icons/hicolor/32x32/apps/gvim.png
install -p -m644 %{SOURCE5} \
   %{buildroot}%{_datadir}/icons/hicolor/48x48/apps/gvim.png
install -p -m644 %{SOURCE6} \
   %{buildroot}%{_datadir}/icons/hicolor/64x64/apps/gvim.png

mkdir -p $RPM_BUILD_ROOT%{_datadir}/metainfo
cat > $RPM_BUILD_ROOT%{_datadir}/metainfo/gvim.appdata.xml <<EOF
<?xml version="1.0" encoding="UTF-8"?>
<!-- Copyright 2014 Richard Hughes <richard@hughsie.com> -->
<!--
EmailAddress: Bram@moolenaar.net>
SentUpstream: 2014-05-22
-->
<component type="desktop-application">
  <id>org.vim.Vim</id>
  <name>GVim</name>
  <metadata_license>CC0-1.0</metadata_license>
  <project_license>Vim</project_license>
  <summary>The VIM version of the vi editor for the X Window System</summary>
  <description>
    <p>
     Vim is an advanced text editor that seeks to provide the power of the
     de-facto Unix editor 'Vi', with a more complete feature set.
     It's useful whether you're already using vi or using a different editor.
    </p>
    <p>
     Vim is a highly configurable text editor built to enable efficient text
     editing.
     Vim is often called a "programmer's editor," and so useful for programming
     that many consider it an entire IDE. It is not just for programmers, though.
     Vim is perfect for all kinds of text editing, from composing email to
     editing configuration files.
    </p>
    <p>
     We ship the current Vim release - %{version}
    </p>
  </description>
  <releases>
    <release version="%{version}" date="%(date +%F -r %{SOURCE0})" />
  </releases>
  <screenshots>
    <screenshot type="default">
      <image>https://raw.githubusercontent.com/zdohnal/vim/zdohnal-screenshot/gvim16_9.png</image>
    </screenshot>
  </screenshots>
  <url type="homepage">http://www.vim.org/</url>
  <content_rating type="oars-1.1"/>
</component>
EOF

appstream-util validate-relax --nonet %{buildroot}/%{_datadir}/metainfo/*.appdata.xml

for i in gvim.1 gex.1 gview.1 vimx.1; do 
  echo ".so man1/vim.1" > %{buildroot}/%{_mandir}/man1/$i
done
echo ".so man1/vimdiff.1" > %{buildroot}/%{_mandir}/man1/gvimdiff.1
echo ".so man1/vimtutor.1" > %{buildroot}/%{_mandir}/man1/gvimtutor.1
%else

rm %{buildroot}/%{_mandir}/man1/evim.*
rm %{buildroot}/%{_datadir}/applications/{vim,gvim}.desktop
rm %{buildroot}/%{_datadir}/icons/{hicolor,locolor}/*/apps/gvim.png
%endif

( cd %{buildroot}
  ln -sf %{_libexecdir}/vi .%{_bindir}/rvi
  ln -sf %{_libexecdir}/vi .%{_bindir}/rview
  ln -sf %{_libexecdir}/vi .%{_bindir}/ex
  ln -sf vim .%{_bindir}/rvim
  ln -sf vim .%{_bindir}/vimdiff
  perl -pi -e "s,%{buildroot},," .%{_mandir}/man1/vim.1 .%{_mandir}/man1/vimtutor.1
  rm -f .%{_mandir}/man1/rvim.1
  cp -p .%{_mandir}/man1/vim.1 .%{_mandir}/man1/vi.1
  ln -sf vi.1.gz .%{_mandir}/man1/rvi.1.gz
  ln -sf vi.1.gz .%{_mandir}/man1/ex.1
  ln -sf vi.1.gz .%{_mandir}/man1/view.1
  ln -sf vi.1.gz .%{_mandir}/man1/rview.1
  ln -sf vim.1.gz .%{_mandir}/man1/vimdiff.1.gz

%if %{with gui}
  ln -sf gvim ./%{_bindir}/gview
  ln -sf gvim ./%{_bindir}/gex
  ln -sf gvim ./%{_bindir}/evim
  ln -sf gvim ./%{_bindir}/gvimdiff
  ln -sf gvim ./%{_bindir}/vimx

  %if "%{desktop_file}" == "1"
    desktop-file-install \
        --dir %{buildroot}/%{_datadir}/applications \
        %{buildroot}/%{_datadir}/applications/gvim.desktop
  %else
    mkdir -p ./%{_sysconfdir}/X11/applnk/Applications
    cp %{buildroot}/%{_datadir}/applications/gvim.desktop ./%{_sysconfdir}/X11/applnk/Applications/gvim.desktop
  %endif

%endif

  ( cd ./%{_datadir}/%{name}/%{vimdir}/lang; \
    ln -sf menu_ja_jp.ujis.vim menu_ja_jp.eucjp.vim )
)

pushd %{buildroot}/%{_datadir}/%{name}/%{vimdir}/tutor
mkdir conv
   iconv -f CP1252 -t UTF8 tutor.ca > conv/tutor.ca
   iconv -f CP1252 -t UTF8 tutor.it > conv/tutor.it
   iconv -f CP1252 -t UTF8 tutor.fr > conv/tutor.fr
   iconv -f CP1252 -t UTF8 tutor.es > conv/tutor.es
   iconv -f CP1252 -t UTF8 tutor.de > conv/tutor.de
   iconv -f UTF8 -t UTF8 tutor.ja.utf-8 > conv/tutor.ja.utf-8
   iconv -f UTF8 -t UTF8 tutor.ko.utf-8 > conv/tutor.ko.utf-8
   iconv -f CP1252 -t UTF8 tutor.no > conv/tutor.no
   iconv -f ISO-8859-2 -t UTF8 tutor.pl > conv/tutor.pl
   iconv -f ISO-8859-2 -t UTF8 tutor.sk > conv/tutor.sk
   iconv -f KOI8R -t UTF8 tutor.ru > conv/tutor.ru
   iconv -f CP1252 -t UTF8 tutor.sv > conv/tutor.sv
   mv -f tutor.ja.euc tutor.ja.sjis tutor.ko.euc tutor.pl.cp1250 tutor.zh.big5 tutor.ru.cp1251 tutor.zh.euc tutor.sr.cp1250 tutor.sr.utf-8 conv/
   rm -f tutor.ca tutor.de tutor.es tutor.fr tutor.gr tutor.it tutor.ja.utf-8 tutor.ko.utf-8 tutor.no tutor.pl tutor.sk tutor.ru tutor.sv
mv -f conv/* .
rmdir conv
popd

chmod 644 %{buildroot}/%{_datadir}/%{name}/%{vimdir}/doc/vim2html.pl \
 %{buildroot}/%{_datadir}/%{name}/%{vimdir}/tools/*.pl \
 %{buildroot}/%{_datadir}/%{name}/%{vimdir}/tools/vim132
chmod 644 ../runtime/doc/vim2html.pl

mkdir -p %{buildroot}%{_sysconfdir}
install -p -m644 %{SOURCE1} %{buildroot}%{_sysconfdir}/virc
install -p -m644 %{SOURCE2} %{buildroot}%{_sysconfdir}/vimrc

%if %{with default_editor}
mkdir -p %{buildroot}/%{_sysconfdir}/profile.d
install -p -m644 %{SOURCE9} %{buildroot}/%{_sysconfdir}/profile.d/vim-default-editor.sh
mkdir -p %{buildroot}/%{_datadir}/fish/vendor_functions.d/
%endif

mkdir -p %{buildroot}%{_rpmconfigdir}/macros.d/
install -p -m644 %{SOURCE8} %{buildroot}%{_rpmconfigdir}/macros.d/

(cd ../runtime; rm -rf doc; ln -svf ../../vim/%{vimdir}/doc docs;) 
rm -f %{buildroot}/%{_datadir}/vim/%{vimdir}/macros/maze/maze*.c
rm -rf %{buildroot}/%{_datadir}/vim/%{vimdir}/tools
rm -rf %{buildroot}/%{_datadir}/vim/%{vimdir}/doc/vim2html.pl
rm -f %{buildroot}/%{_datadir}/vim/%{vimdir}/tutor/tutor.gr.utf-8~

for i in pl.ISO8859-2 it.ISO8859-1 ru.KOI8-R fr.ISO8859-1 da.ISO8859-1 de.ISO8859-1 tr.ISO8859-9; do
  rm -rf %{buildroot}/%{_mandir}/$i
done

mv %{buildroot}/%{_mandir}/ru.UTF-8 %{buildroot}/%{_mandir}/ru

for i in fr.UTF-8 it.UTF-8 pl.UTF-8 da.UTF-8 de.UTF-8 tr.UTF-8; do
  rm -rf %{buildroot}/%{_mandir}/$i
done

echo ".so man1/vim.1" > %{buildroot}/%{_mandir}/man1/rvim.1

mkdir -p %{buildroot}/%{_mandir}/man5
echo ".so man1/vim.1" > %{buildroot}/%{_mandir}/man5/vimrc.5
echo ".so man1/vi.1" > %{buildroot}/%{_mandir}/man5/virc.5
touch %{buildroot}/%{_datadir}/%{name}/vimfiles/doc/tags



%transfiletriggerin common -- %{_datadir}/%{name}/vimfiles/doc
%{_bindir}/vim -c ":helptags %{_datadir}/%{name}/vimfiles/doc" -c :q &> /dev/null || :

%transfiletriggerpostun common -- %{_datadir}/%{name}/vimfiles/doc
> %{_datadir}/%{name}/vimfiles/doc/tags || :
%{_bindir}/vim -c ":helptags %{_datadir}/%{name}/vimfiles/doc" -c :q &> /dev/null || :

%check
# Test_exrc will hung terminal
export TEST_SKIP_PAT='\(Test_exrc\)'
export TERM=xterm
LC_ALL=en_US.UTF-8  make test || echo "Warning: tests have failure."


%files common
%config(noreplace) %{_sysconfdir}/vimrc
%{!?_licensedir:%global license %%doc}
%doc README*
%doc runtime/docs
%{_datadir}/%{name}/%{vimdir}/autoload
%{_datadir}/%{name}/%{vimdir}/colors
%{_datadir}/%{name}/%{vimdir}/compiler
%{_datadir}/%{name}/%{vimdir}/pack
%{_datadir}/%{name}/%{vimdir}/doc
%{_datadir}/%{name}/%{vimdir}/*.vim
%exclude %{_datadir}/%{name}/%{vimdir}/defaults.vim
%{_datadir}/%{name}/%{vimdir}/ftplugin
%{_datadir}/%{name}/%{vimdir}/import/dist/vimhelp.vim
%{_datadir}/%{name}/%{vimdir}/import/dist/vimhighlight.vim
%{_datadir}/%{name}/%{vimdir}/indent
%{_datadir}/%{name}/%{vimdir}/keymap
%{_datadir}/%{name}/%{vimdir}/lang/*.vim
%{_datadir}/%{name}/%{vimdir}/lang/*.txt
%dir %{_datadir}/%{name}/%{vimdir}/lang
%{_datadir}/%{name}/%{vimdir}/macros
%{_datadir}/%{name}/%{vimdir}/plugin
%{_datadir}/%{name}/%{vimdir}/print
%{_datadir}/%{name}/%{vimdir}/syntax
%{_datadir}/%{name}/%{vimdir}/tutor

%if ! %{withvimspell}
%{_datadir}/%{name}/%{vimdir}/spell
%endif

%lang(af) %{_datadir}/%{name}/%{vimdir}/lang/af
%lang(ca) %{_datadir}/%{name}/%{vimdir}/lang/ca
%lang(cs) %{_datadir}/%{name}/%{vimdir}/lang/cs
%lang(cs.cp1250) %{_datadir}/%{name}/%{vimdir}/lang/cs.cp1250
%lang(da) %{_datadir}/%{name}/%{vimdir}/lang/da
%lang(de) %{_datadir}/%{name}/%{vimdir}/lang/de
%lang(en_GB) %{_datadir}/%{name}/%{vimdir}/lang/en_GB
%lang(eo) %{_datadir}/%{name}/%{vimdir}/lang/eo
%lang(es) %{_datadir}/%{name}/%{vimdir}/lang/es
%lang(fi) %{_datadir}/%{name}/%{vimdir}/lang/fi
%lang(fr) %{_datadir}/%{name}/%{vimdir}/lang/fr
%lang(ga) %{_datadir}/%{name}/%{vimdir}/lang/ga
%lang(it) %{_datadir}/%{name}/%{vimdir}/lang/it
%lang(ja) %{_datadir}/%{name}/%{vimdir}/lang/ja
%lang(ja.euc-jp) %{_datadir}/%{name}/%{vimdir}/lang/ja.euc-jp
%lang(ja.sjis) %{_datadir}/%{name}/%{vimdir}/lang/ja.sjis
%lang(ko) %{_datadir}/%{name}/%{vimdir}/lang/ko
%lang(ko) %{_datadir}/%{name}/%{vimdir}/lang/ko.UTF-8
%lang(lv) %{_datadir}/%{name}/%{vimdir}/lang/lv
%lang(nb) %{_datadir}/%{name}/%{vimdir}/lang/nb
%lang(nl) %{_datadir}/%{name}/%{vimdir}/lang/nl
%lang(no) %{_datadir}/%{name}/%{vimdir}/lang/no
%lang(pl) %{_datadir}/%{name}/%{vimdir}/lang/pl
%lang(pl.UTF-8) %{_datadir}/%{name}/%{vimdir}/lang/pl.UTF-8
%lang(pl.cp1250) %{_datadir}/%{name}/%{vimdir}/lang/pl.cp1250
%lang(pt_BR) %{_datadir}/%{name}/%{vimdir}/lang/pt_BR
%lang(ru) %{_datadir}/%{name}/%{vimdir}/lang/ru
%lang(ru.cp1251) %{_datadir}/%{name}/%{vimdir}/lang/ru.cp1251
%lang(sk) %{_datadir}/%{name}/%{vimdir}/lang/sk
%lang(sk.cp1250) %{_datadir}/%{name}/%{vimdir}/lang/sk.cp1250
%lang(sr) %{_datadir}/%{name}/%{vimdir}/lang/sr
%lang(sv) %{_datadir}/%{name}/%{vimdir}/lang/sv
%lang(tr) %{_datadir}/%{name}/%{vimdir}/lang/tr
%lang(uk) %{_datadir}/%{name}/%{vimdir}/lang/uk
%lang(uk.cp1251) %{_datadir}/%{name}/%{vimdir}/lang/uk.cp1251
%lang(vi) %{_datadir}/%{name}/%{vimdir}/lang/vi
%lang(zh_CN) %{_datadir}/%{name}/%{vimdir}/lang/zh_CN
%lang(zh_CN.cp936) %{_datadir}/%{name}/%{vimdir}/lang/zh_CN.cp936
%lang(zh_TW) %{_datadir}/%{name}/%{vimdir}/lang/zh_TW
%lang(zh_CN.UTF-8) %{_datadir}/%{name}/%{vimdir}/lang/zh_CN.UTF-8
%lang(zh_TW.UTF-8) %{_datadir}/%{name}/%{vimdir}/lang/zh_TW.UTF-8
/%{_bindir}/xxd
%{_mandir}/man1/rvim.*
%{_mandir}/man1/vim.*
%{_mandir}/man1/vimdiff.*
%{_mandir}/man1/vimtutor.*
%{_mandir}/man1/xxd.*
%{_mandir}/man5/vimrc.*

%if %{with gui}
%{_mandir}/man1/gex.*
%{_mandir}/man1/gview.*
%{_mandir}/man1/gvim*
%{_mandir}/man1/vimx.*
%endif

%lang(fr) %{_mandir}/fr/man1/*
%lang(da) %{_mandir}/da/man1/*
%lang(de) %{_mandir}/de/man1/*
%lang(it) %{_mandir}/it/man1/*
%lang(ja) %{_mandir}/ja/man1/*
%lang(pl) %{_mandir}/pl/man1/*
%lang(ru) %{_mandir}/ru/man1/*
%lang(tr) %{_mandir}/tr/man1/*

%if %{withvimspell}
%files spell
%dir %{_datadir}/%{name}/%{vimdir}/spell
%{_datadir}/%{name}/vim70/spell/cleanadd.vim
%lang(af) %{_datadir}/%{name}/%{vimdir}/spell/af.*
%lang(am) %{_datadir}/%{name}/%{vimdir}/spell/am.*
%lang(bg) %{_datadir}/%{name}/%{vimdir}/spell/bg.*
%lang(ca) %{_datadir}/%{name}/%{vimdir}/spell/ca.*
%lang(cs) %{_datadir}/%{name}/%{vimdir}/spell/cs.*
%lang(cy) %{_datadir}/%{name}/%{vimdir}/spell/cy.*
%lang(da) %{_datadir}/%{name}/%{vimdir}/spell/da.*
%lang(de) %{_datadir}/%{name}/%{vimdir}/spell/de.*
%lang(el) %{_datadir}/%{name}/%{vimdir}/spell/el.*
%lang(en) %{_datadir}/%{name}/%{vimdir}/spell/en.*
%lang(eo) %{_datadir}/%{name}/%{vimdir}/spell/eo.*
%lang(es) %{_datadir}/%{name}/%{vimdir}/spell/es.*
%lang(fo) %{_datadir}/%{name}/%{vimdir}/spell/fo.*
%lang(fr) %{_datadir}/%{name}/%{vimdir}/spell/fr.*
%lang(ga) %{_datadir}/%{name}/%{vimdir}/spell/ga.*
%lang(gd) %{_datadir}/%{name}/%{vimdir}/spell/gd.*
%lang(gl) %{_datadir}/%{name}/%{vimdir}/spell/gl.*
%lang(he) %{_datadir}/%{name}/%{vimdir}/spell/he.*
%lang(hr) %{_datadir}/%{name}/%{vimdir}/spell/hr.*
%lang(hu) %{_datadir}/%{name}/%{vimdir}/spell/hu.*
%lang(id) %{_datadir}/%{name}/%{vimdir}/spell/id.*
%lang(it) %{_datadir}/%{name}/%{vimdir}/spell/it.*
%lang(ku) %{_datadir}/%{name}/%{vimdir}/spell/ku.*
%lang(la) %{_datadir}/%{name}/%{vimdir}/spell/la.*
%lang(lt) %{_datadir}/%{name}/%{vimdir}/spell/lt.*
%lang(lv) %{_datadir}/%{name}/%{vimdir}/spell/lv.*
%lang(mg) %{_datadir}/%{name}/%{vimdir}/spell/mg.*
%lang(mi) %{_datadir}/%{name}/%{vimdir}/spell/mi.*
%lang(ms) %{_datadir}/%{name}/%{vimdir}/spell/ms.*
%lang(nb) %{_datadir}/%{name}/%{vimdir}/spell/nb.*
%lang(nl) %{_datadir}/%{name}/%{vimdir}/spell/nl.*
%lang(nn) %{_datadir}/%{name}/%{vimdir}/spell/nn.*
%lang(ny) %{_datadir}/%{name}/%{vimdir}/spell/ny.*
%lang(pl) %{_datadir}/%{name}/%{vimdir}/spell/pl.*
%lang(pt) %{_datadir}/%{name}/%{vimdir}/spell/pt.*
%lang(ro) %{_datadir}/%{name}/%{vimdir}/spell/ro.*
%lang(ru) %{_datadir}/%{name}/%{vimdir}/spell/ru.*
%lang(rw) %{_datadir}/%{name}/%{vimdir}/spell/rw.*
%lang(sk) %{_datadir}/%{name}/%{vimdir}/spell/sk.*
%lang(sl) %{_datadir}/%{name}/%{vimdir}/spell/sl.*
%lang(sr) %{_datadir}/%{name}/%{vimdir}/spell/sr.*
%lang(sv) %{_datadir}/%{name}/%{vimdir}/spell/sv.*
%lang(sw) %{_datadir}/%{name}/%{vimdir}/spell/sw.*
%lang(tet) %{_datadir}/%{name}/%{vimdir}/spell/tet.*
%lang(th) %{_datadir}/%{name}/%{vimdir}/spell/th.*
%lang(tl) %{_datadir}/%{name}/%{vimdir}/spell/tl.*
%lang(tn) %{_datadir}/%{name}/%{vimdir}/spell/tn.*
%lang(uk) %{_datadir}/%{name}/%{vimdir}/spell/uk.*
%lang(yi) %{_datadir}/%{name}/%{vimdir}/spell/yi.*
%lang(yi-tr) %{_datadir}/%{name}/%{vimdir}/spell/yi-tr.*
%lang(zu) %{_datadir}/%{name}/%{vimdir}/spell/zu.*
%endif

%files minimal
%config(noreplace) %{_sysconfdir}/virc
%{_bindir}/ex
%{_bindir}/rvi
%{_bindir}/rview
%{_bindir}/vi
%{_bindir}/view
%{_libexecdir}/vi
%{_mandir}/man1/vi.*
%{_mandir}/man1/ex.*
%{_mandir}/man1/rvi.*
%{_mandir}/man1/rview.*
%{_mandir}/man1/view.*
%{_mandir}/man5/virc.*

%files enhanced
%{_bindir}/rvim
%{_bindir}/vim
%{_bindir}/vimdiff
%{_bindir}/vimtutor

%files filesystem
%{_rpmconfigdir}/macros.d/macros.vim
%dir %{_datadir}/%{name}/vimfiles/after
%dir %{_datadir}/%{name}/vimfiles/after/*
%dir %{_datadir}/%{name}/vimfiles/autoload
%dir %{_datadir}/%{name}/vimfiles/colors
%dir %{_datadir}/%{name}/vimfiles/compiler
%dir %{_datadir}/%{name}/vimfiles/doc
%ghost %{_datadir}/%{name}/vimfiles/doc/tags
%dir %{_datadir}/%{name}/vimfiles/ftdetect
%dir %{_datadir}/%{name}/vimfiles/ftplugin
%dir %{_datadir}/%{name}/%{vimdir}/import
%dir %{_datadir}/%{name}/%{vimdir}/import/dist
%dir %{_datadir}/%{name}/vimfiles/indent
%dir %{_datadir}/%{name}/vimfiles/keymap
%dir %{_datadir}/%{name}/vimfiles/lang
%dir %{_datadir}/%{name}/vimfiles/plugin
%dir %{_datadir}/%{name}/vimfiles/print
%dir %{_datadir}/%{name}/vimfiles/spell
%dir %{_datadir}/%{name}/vimfiles/syntax
%dir %{_datadir}/%{name}/vimfiles/tutor

%if %{with gui}
%files X11
%{_bindir}/gvimtutor
%{_bindir}/gvim
%{_bindir}/gvimdiff
%{_bindir}/gview
%{_bindir}/gex
%{_bindir}/vimtutor
%{_bindir}/vimx
%{_bindir}/evim
%{_mandir}/man1/evim.*
%dir %{_datadir}/icons/hicolor
%dir %{_datadir}/icons/hicolor/*
%dir %{_datadir}/icons/hicolor/*/apps
%{_datadir}/icons/hicolor/*/apps/*
%dir %{_datadir}/icons/locolor
%dir %{_datadir}/icons/locolor/*
%dir %{_datadir}/icons/locolor/*/apps
%{_datadir}/icons/locolor/*/apps/*

%if "%{desktop_file}" == "1"
%{_datadir}/metainfo/*.appdata.xml
/%{_datadir}/applications/*
%exclude /%{_datadir}/applications/vim.desktop
%else
/%{_sysconfdir}/X11/applnk/*/gvim.desktop
%endif

%endif

%files data
%license LICENSE
%dir %{_datadir}/%{name}
%dir %{_datadir}/%{name}/%{vimdir}
%{_datadir}/%{name}/%{vimdir}/defaults.vim
%dir %{_datadir}/%{name}/vimfiles

%if %{with default_editor}
%files default-editor
%dir %{_datadir}/fish/vendor_conf.d
%{_datadir}/fish/vendor_conf.d/vim-default-editor.fish
%config(noreplace) %{_sysconfdir}/profile.d/vim-default-editor.*
%endif



%changelog
* Fri Feb 28 2025 Zhao Zhen <jeremiazhao@tencent.com> - 9.0.2092-10
- fixed CVE-2025-1215 CVE-2025-26603

* Thu Jan 16 2025 Yi Lin <nilusyi@tencent.com> - 9.0.2092-9
- fixed CVE-2025-22134

* Tue Oct 08 2024 Yi Lin <nilusyi@tencent.com> - 9.0.2092-8
- fixed CVE-2024-47814

* Thu Sep 26 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 9.0.2092-7
- Rebuilt for clarifying the packages requirement in BaseOS and AppStream

* Thu Sep 05 2024 Yi Lin <nilusyi@tencent.com> - 9.0.2092-6
- fixed CVE-2024-43802

* Wed Aug 21 2024 Yi Lin <nilusyi@tencent.com> - 9.0.2092-5
- fixed CVE-2024-43374

* Fri Aug 16 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 9.0.2092-4
- Rebuilt for loongarch release

* Wed Aug 07 2024 Yi Lin <nilusyi@tencent.com> - 9.0.2092-3
- fixed CVE-2024-41957

* Tue Feb 20 2024 Yi Lin <nilusyi@tencent.com> - 9.0.2092-2
- fixed CVE-2023-48231 CVE-2023-48232 CVE-2023-48233 CVE-2023-48234 CVE-2023-48235 
  CVE-2023-48236 CVE-2023-48237 CVE-2023-48706 CVE-2024-22667 

* Wed Nov 08 2023 Zhao Zhen <jeremiazhao@tencent.com> - 9.0.2092-1
- updated to upstream version 9.0.2092
- fixed CVE-2023-4735 CVE-2023-4736 CVE-2023-4734 CVE-2023-4738 CVE-2023-4752 
  CVE-2023-4781 CVE-2023-4733 CVE-2023-4750 CVE-2023-5344 CVE-2023-5441 CVE-2023-5535

* Fri Sep 08 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 9.0.1677-3
- Rebuilt for OpenCloudOS Stream 23.09

* Wed Aug 16 2023 Zhao Zhen <jeremiazhao@tencent.com> - 9.0.1677-2
- small fix

* Wed Aug 16 2023 kianli <kianli@tencent.com> - 9.0.1677-1
- Upgrade to 9.0.1677 and fix bogus date in changelog

* Thu Jul 27 2023 Zhao Zhen <jeremiazhao@tencent.com> - 9.0.1672-2
- disabled auto insert comments by default

* Thu Jul 13 2023 Zhao Zhen <jeremiazhao@tencent.com> - 9.0.1672-1
- updated to 9.0.1672

* Fri Apr 28 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 9.0.1367-3
- Rebuilt for OpenCloudOS Stream 23.05

* Fri Mar 31 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 9.0.1367-2
- Rebuilt for OpenCloudOS Stream 23

* Tue Mar 07 2023 Zhao Zhen <jeremiazhao@tencent.com> - 9.0.1367-1
- updated to 9.0.1367

* Wed Dec 28 2022 Shuo Wang <abushwang@tencent.com> - 8.2.4922-2
- update vimrc

* Mon May  9 2022 Songqiao Tao <joeytao@tencent.com> - 8.2.4922-1
- patchlevel 4922

* Sat May  7 2022 Songqiao Tao <joeytao@tencent.com> - 8.2.4897-1
- initial build
